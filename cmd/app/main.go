package main

import (
	"context"
	"go.uber.org/zap"
	"os"
	"processium/payment/internal/config"
	currencyrepo "processium/payment/internal/currency/impl"
	invoicerepo "processium/payment/internal/invoice/impl"
	networkrepo "processium/payment/internal/network/impl"
	paymentserver "processium/payment/internal/server"
	paymentservice "processium/payment/internal/service"
	postgresrepo "processium/payment/internal/storage/postgres/impl"
	"processium/payment/internal/storage/postgres/utils"
	tmpwalletrepo "processium/payment/internal/tmpwallet/impl"
	vipercfg "processium/payment/pkg/utils/config"
	"processium/payment/pkg/watchdog"
)

const ConfigEnv = "PAYMENT_CONFIG"

const AppName = "payment"

func main() {
	ctx := context.TODO()

	logger, err := zap.NewProduction(zap.AddStacktrace(zap.PanicLevel))
	if err != nil {
		panic("zap init failed.")
	}
	zap.ReplaceGlobals(logger)
	zap.L().Sync()

	configPath, ok := os.LookupEnv(ConfigEnv)
	if !ok {
		zap.L().Panic("Could not get config path. Variable is not set.", zap.String("variable", ConfigEnv))
	}

	viper, err := vipercfg.LoadViper(AppName, configPath)
	if err != nil {
		zap.L().Fatal("Unable to load Viper!", zap.Error(err))
	}
	zap.L().Info("Loaded Viper successfully!")

	cfg, err := config.ParseConfig(viper)
	if err != nil {
		zap.L().Fatal("Unable to parse config!", zap.Error(err))
	}
	zap.L().Info("Loaded config successfully!")

	pgDB, err := utils.NewPGDb(cfg.PostgresConfig)
	if err != nil {
		zap.L().Fatal("Unable connect to postgres!", zap.Error(err))
	}
	zap.L().Info("Postgres DB initialised successfully!")

	pgStorage, err := postgresrepo.NewPGStorage(pgDB)
	if err != nil {
		zap.L().Fatal("Postgres DB initialisation error!", zap.Error(err))
	}
	zap.L().Info("Postgres storage initialised successfully!")
	defer pgStorage.Close()

	currencyrepo.InitGlobalRepository(pgDB)
	zap.L().Info("Currency storage initialised successfully!")

	networkrepo.InitGlobalRepository(pgDB)
	zap.L().Info("Network storage initialised successfully!")

	invoicerepo.InitGlobalRepository(pgDB)
	zap.L().Info("Invoice storage initialised successfully!")

	tmpwalletrepo.InitGlobalRepository(pgDB)
	zap.L().Info("Tmp wallet storage initialised successfully!")

	watchdog.InitGlobal(pgDB)
	watchdog.W().Start(ctx)
	zap.L().Info("Watchdog started!")

	service := paymentservice.NewService(pgStorage)
	err = service.Init()
	if err != nil {
		zap.L().Fatal("Service initialisation error!", zap.Error(err))
	}
	zap.L().Info("Service initialised successfully!")

	server := paymentserver.NewServer(cfg.ServerConfig, service)
	if err = server.Run(ctx); err != nil {
		zap.L().Fatal("Error while starting server!", zap.Error(err))
	}
}
