package calculations

import (
	"github.com/shopspring/decimal"
	"math/big"
)

func DecimalAbs(d decimal.Decimal) decimal.Decimal {
	var minusOne = decimal.NewFromInt(-1)
	return decimal.Max(d, d.Mul(minusOne))
}

func DecimalDelimeterByDigits(digits int32) decimal.Decimal {
	return decimal.NewFromBigInt(big.NewInt(1), digits)
}
