package errors

import "errors"

func AnyOf(err error, errs ...error) bool {
	for _, v := range errs {
		if errors.Is(err, v) {
			return true
		}
	}
	return false
}
