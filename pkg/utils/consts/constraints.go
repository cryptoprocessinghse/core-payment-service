package consts

import (
	"github.com/shopspring/decimal"
	"time"
)

var (
	MinExpectedAmount  = decimal.NewFromInt(0)
	MinTotalCommission = decimal.NewFromInt(0)
)

const (
	DefaultMaxRetries = 240
	DefaultDelay      = time.Second * 30
	MinDelay          = time.Second * 5
)
