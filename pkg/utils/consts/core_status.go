package consts

type CoreStatusAlias string

const (
	Awaiting  CoreStatusAlias = "awaiting"
	Held      CoreStatusAlias = "held"
	Sending   CoreStatusAlias = "sending"
	Succeed   CoreStatusAlias = "succeed"
	Clear     CoreStatusAlias = "clear"
	Failed    CoreStatusAlias = "failed"
	Cancelled CoreStatusAlias = "cancelled"
)

func StatusesToBeCollected() []CoreStatusAlias {
	return []CoreStatusAlias{Awaiting, Held, Sending, Succeed}
}
