package consts

type CurrencyAlias string

const (
	BNB       CurrencyAlias = "BNB"
	ETHGoerli CurrencyAlias = "ETHG"
	ETH       CurrencyAlias = "ETH"
)
