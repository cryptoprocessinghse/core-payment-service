package server

import "github.com/gofiber/fiber/v2"

func PingHandler() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		_, err := ctx.Write([]byte("pong"))
		return err
	}
}
