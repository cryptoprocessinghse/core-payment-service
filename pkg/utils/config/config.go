package config

import (
	"github.com/spf13/viper"
)

func LoadViper(appName, path string) (*viper.Viper, error) {
	viperInstance := viper.New()
	viperInstance.SetEnvPrefix(appName)
	viperInstance.SetConfigFile(path)
	viperInstance.AutomaticEnv()
	return viperInstance, viperInstance.ReadInConfig()
}
