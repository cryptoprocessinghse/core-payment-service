package watchdog

import "github.com/shopspring/decimal"

type InspectResponse struct {
	WalletID       int
	Amount         decimal.Decimal
	ExpectedAmount decimal.Decimal
}
