package watchdog

import "errors"

var (
	ErrUnknownTaskType = errors.New("unknown task type")
	ErrInspectError    = errors.New("inspect error")
)
