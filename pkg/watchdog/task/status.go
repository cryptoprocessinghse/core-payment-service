package task

type Status int

const (
	Idle Status = iota
	Completed
	Failed
)
