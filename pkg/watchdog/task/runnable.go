package task

type Runnable interface {
	Run() error
	Runnables() []Runnable
}
