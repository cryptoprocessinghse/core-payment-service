package task

import (
	"sync"
	"time"
)

type Repeater struct {
	delay      time.Duration
	maxRetries int
	retries    int
	lastCheck  time.Time

	rtmu *sync.RWMutex
}

// NewRepeater maxRetries = 0 means no limit for retries, otherwise - it's the real limit for retry attempt.
func NewRepeater(maxRetries int, minDelay time.Duration) Repeater {
	return Repeater{
		maxRetries: maxRetries,
		delay:      minDelay,
		retries:    0,
		lastCheck:  time.Unix(0, 0),
		rtmu:       new(sync.RWMutex),
	}
}

func (rt *Repeater) IsReady() bool {
	rt.rtmu.RLock()
	defer rt.rtmu.RUnlock()
	return time.Now().After(rt.lastCheck.Add(rt.delay))
}

func (rt *Repeater) Iter() error {
	if !rt.IsReady() {
		return ErrNotReady
	}

	rt.rtmu.Lock()
	defer rt.rtmu.Unlock()

	rt.lastCheck = time.Now()
	if rt.maxRetries != 0 && rt.retries >= rt.maxRetries {
		return ErrMaxRetriesExceeded
	}
	rt.retries += 1
	return nil
}

// Getters

func (rt *Repeater) Retries() int {
	rt.rtmu.RLock()
	defer rt.rtmu.RUnlock()
	return rt.retries
}

func (rt *Repeater) MaxRetries() int {
	rt.rtmu.RLock()
	defer rt.rtmu.RUnlock()
	return rt.maxRetries
}

func (rt *Repeater) LastCheck() time.Time {
	rt.rtmu.RLock()
	defer rt.rtmu.RUnlock()
	return rt.lastCheck
}

func (rt *Repeater) Delay() time.Duration {
	rt.rtmu.RLock()
	defer rt.rtmu.RUnlock()
	return rt.delay
}
