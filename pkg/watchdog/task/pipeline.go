package task

import "sync"

var _ Runnable = (*Pipeline)(nil)

type Pipeline struct {
	runnables []Runnable
	curIndex  int

	rwmu *sync.RWMutex
}

func NewPipeline(runnables ...Runnable) *Pipeline {
	return &Pipeline{
		runnables: runnables,
		curIndex:  0,
		rwmu:      new(sync.RWMutex),
	}
}

func (p *Pipeline) next() Runnable {
	p.rwmu.Lock()
	defer p.rwmu.Unlock()

	if p.curIndex >= len(p.runnables)-1 {
		return nil
	}

	p.curIndex += 1
	return p.runnables[p.curIndex]
}

func (p *Pipeline) Runnables() []Runnable {
	p.rwmu.RLock()
	defer p.rwmu.RUnlock()

	result := p.runnables
	return result
}

func (p *Pipeline) current() Runnable {
	p.rwmu.RLock()
	defer p.rwmu.RUnlock()

	if p.curIndex >= len(p.runnables) {
		return nil
	}

	return p.runnables[p.curIndex]
}

func (p *Pipeline) Extend(runnable Runnable) {
	runnables := runnable.Runnables()

	p.rwmu.Lock()
	defer p.rwmu.Unlock()
	p.runnables = append(p.runnables, runnables...)
}

func (p *Pipeline) Run() error {
	cur := p.current()
	if cur == nil {
		return ErrPipelineFinished
	}

	err := cur.Run()
	if err == nil {
		if p.next() == nil {
			return ErrPipelineFinished
		}
		return ErrPipelineInProgress
	}

	return err
}
