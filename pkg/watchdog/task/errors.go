package task

import "errors"

var (
	ErrMaxRetriesExceeded = errors.New("max retries exceeded")
	ErrNotReady           = errors.New("task is not ready")

	// ErrPipelineFinished не столько ошибка, сколько быстрая проверка на окончание пайплана.
	ErrPipelineFinished   = errors.New("pipeline finished")
	ErrPipelineInProgress = errors.New("pipeline in progress")
)
