package watchdog

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/realTristan/goqueue"
	"go.uber.org/zap"
	errutils "processium/payment/pkg/utils/errors"
	"processium/payment/pkg/watchdog/task"
	"sync"
)

const (
	watchdogLogLabel = "[Watchdog]"
)

var (
	globalWatchdog *Watchdog
	mu             sync.RWMutex
)

func InitGlobal(db *sqlx.DB) *Watchdog {
	mu.Lock()
	defer mu.Unlock()

	globalWatchdog = &Watchdog{
		queue: goqueue.Create(),
		db:    db,
	}

	// TODO:
	//err := globalWatchdog.collectTasks()
	//if err != nil {
	//	zap.L().Fatal("Watchdog initialisation failed!", zap.Error(err))
	//}

	return globalWatchdog
}

func W() *Watchdog {
	mu.RLock()
	w := globalWatchdog
	mu.RUnlock()
	return w
}

type Watchdog struct {
	db    *sqlx.DB
	queue *goqueue.ItemQueue
}

func (w *Watchdog) PushBack(runnable task.Runnable) bool {
	if runnable == nil {
		return false
	}
	w.queue.Put(runnable)
	return true
}

func (w *Watchdog) IsEmpty() bool {
	return w.queue.IsEmpty()
}

func (w *Watchdog) Size() int {
	return w.queue.Size()
}

func (w *Watchdog) PullFront() task.Runnable {
	res, ok := w.queue.Grab().(task.Runnable)
	if ok {
		return res
	}

	return nil
}

// Start starts goroutine which executes tasks in queue.
func (w *Watchdog) Start(ctx context.Context) {
	go w._start(ctx)
}

func (w *Watchdog) _start(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		if w.IsEmpty() {
			continue
		}

		runnable := w.PullFront()
		if runnable == nil {
			continue
		}

		err := runnable.Run()

		if err != nil && !errutils.AnyOf(err, task.ErrPipelineFinished, task.ErrPipelineInProgress, task.ErrNotReady) {
			zap.L().Error(fmt.Sprintf("%s error", watchdogLogLabel), zap.Error(err))
		}

		if err != nil && !errutils.AnyOf(err, task.ErrMaxRetriesExceeded, task.ErrPipelineFinished) {
			w.PushBack(runnable)
			continue
		}
	}
}
