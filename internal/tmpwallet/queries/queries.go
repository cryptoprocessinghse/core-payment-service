package queries

const UpdateStatus = `UPDATE wallets.temporary_wallet SET status = $2 WHERE id = $1;`

const SelectByStatusList = `SELECT * FROM wallets.temporary_wallet WHERE status = ANY($1);`
