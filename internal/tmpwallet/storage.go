package tmpwallet

import (
	"context"
	"processium/payment/internal/models"
	"processium/payment/pkg/utils/consts"
)

type Storage interface {
	read
}

type read interface {
	UpdateStatus(ctx context.Context, walletID int64, newStatus consts.CoreStatusAlias) error
	SelectByStatusList(ctx context.Context, statuses []consts.CoreStatusAlias) ([]*models.TemporaryWallet, error)
}
