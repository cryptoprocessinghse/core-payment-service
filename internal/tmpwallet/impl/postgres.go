package impl

import (
	"context"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"processium/payment/internal/models"
	"processium/payment/internal/tmpwallet"
	"processium/payment/internal/tmpwallet/queries"
	"processium/payment/pkg/utils/consts"
	"sync"
)

var (
	GlobalRepository *PgRepositoryImpl
	creationMutex    = &sync.Mutex{}
)

func NewRepository(db *sqlx.DB) *PgRepositoryImpl {
	return &PgRepositoryImpl{DB: db}
}

func InitGlobalRepository(db *sqlx.DB) *PgRepositoryImpl {
	creationMutex.Lock()
	defer creationMutex.Unlock()

	if GlobalRepository == nil {
		GlobalRepository = NewRepository(db)
	}

	return GlobalRepository
}

type PgRepositoryImpl struct {
	DB *sqlx.DB
}

func (r *PgRepositoryImpl) UpdateStatus(ctx context.Context, walletID int64, newStatus consts.CoreStatusAlias) error {
	_, err := r.DB.ExecContext(ctx, queries.UpdateStatus, walletID, newStatus)
	if err != nil {
		return err
	}

	return nil
}

func (r *PgRepositoryImpl) SelectByStatusList(ctx context.Context, statuses []consts.CoreStatusAlias) ([]*models.TemporaryWallet, error) {
	var tWallets []*models.TemporaryWallet

	if err := r.DB.Select(&tWallets, queries.SelectByStatusList, pq.Array(statuses)); err != nil {
		return nil, err
	}

	return tWallets, nil
}

var _ tmpwallet.Storage = (*PgRepositoryImpl)(nil)
