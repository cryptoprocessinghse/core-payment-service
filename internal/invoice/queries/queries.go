package queries

const GetMerchantWalletAddressByInvoiceID = `
	SELECT merchant_wallet.wallet_address FROM processing.invoice
	INNER JOIN wallets.merchant_wallet ON invoice.merchant_wallet_id = merchant_wallet.id
	WHERE invoice.id=$1;`
