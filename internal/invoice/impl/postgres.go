package impl

import (
	"context"
	"database/sql"
	"errors"
	"github.com/guregu/null"
	"github.com/jmoiron/sqlx"
	"processium/payment/internal/invoice"
	invoicerrors "processium/payment/internal/invoice/errors"
	"processium/payment/internal/invoice/queries"
	"sync"
)

var (
	GlobalRepository *pgRepositoryImpl
	creationMutex    = &sync.Mutex{}
)

func NewRepository(db *sqlx.DB) *pgRepositoryImpl {
	return &pgRepositoryImpl{DB: db}
}

func InitGlobalRepository(db *sqlx.DB) *pgRepositoryImpl {
	creationMutex.Lock()
	defer creationMutex.Unlock()

	if GlobalRepository == nil {
		GlobalRepository = NewRepository(db)
	}

	return GlobalRepository
}

type pgRepositoryImpl struct {
	DB *sqlx.DB
}

func (r *pgRepositoryImpl) GetMerchantWalletAddressByInvoiceID(ctx context.Context, invoiceID int64) (null.String, error) {
	var address null.String
	err := r.DB.GetContext(ctx, &address, queries.GetMerchantWalletAddressByInvoiceID, invoiceID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return null.String{}, invoicerrors.ErrMerchantWalletNotFound
		}
		return null.String{}, err
	}
	return address, err
}

var _ invoice.Storage = (*pgRepositoryImpl)(nil)
