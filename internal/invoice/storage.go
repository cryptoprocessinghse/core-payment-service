package invoice

import (
	"context"
	"github.com/guregu/null"
)

type Storage interface {
	read
}

type read interface {
	GetMerchantWalletAddressByInvoiceID(ctx context.Context, invoiceID int64) (null.String, error)
}
