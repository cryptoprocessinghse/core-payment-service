package errors

import "errors"

var (
	ErrMerchantWalletNotFound = errors.New("merchant wallet not found")
)
