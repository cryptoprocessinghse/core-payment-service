package models

import (
	"github.com/guregu/null"
	"github.com/shopspring/decimal"
	"processium/payment/pkg/utils/consts"
)

type TemporaryWallet struct {
	ID              null.Int               `db:"id"`
	Address         null.String            `db:"wallet_address"`
	MerchantAddress null.String            `db:"merchant_address"`
	PrivateKey      null.String            `db:"private_key"`
	Currency        consts.CurrencyAlias   `db:"currency"`
	Status          consts.CoreStatusAlias `db:"status"`
	ExpectedAmount  decimal.NullDecimal    `db:"expected_amount"`
	CallbackURL     null.String            `db:"callback_url"`
	TotalCommission decimal.NullDecimal    `db:"total_commission"`
}

type TemporaryWalletInfo struct {
	ID      null.Int               `json:"id"`
	Address null.String            `json:"address"`
	Status  consts.CoreStatusAlias `json:"status"`
	Balance Balance                `json:"balance,omitempty"`
}

type Balance struct {
	Currency consts.CurrencyAlias `json:"currency"`
	Amount   decimal.Decimal      `json:"amount"`
}
