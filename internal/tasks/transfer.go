package tasks

import (
	"context"
	"errors"
	"github.com/shopspring/decimal"
	"go.uber.org/zap"
	"processium/payment/internal/processiumcrypto"
	tmpwalletrepo "processium/payment/internal/tmpwallet/impl"
	"processium/payment/pkg/utils/consts"
	"processium/payment/pkg/watchdog/task"
	"sync"
	"time"
)

var _ task.Runnable = (*TransferTask)(nil)

type TransferTask struct {
	task.Repeater
	walletID        int64
	from            processiumcrypto.Wallet
	to              string
	amount          decimal.Decimal
	callbackURL     string
	statusOnSuccess consts.CoreStatusAlias
	statusOnFail    consts.CoreStatusAlias

	mu *sync.RWMutex
}

func NewTransferTask(walletID int64, fromWallet processiumcrypto.Wallet, toAddress string, amount decimal.Decimal, statusOnSuccess consts.CoreStatusAlias, statusOnFail consts.CoreStatusAlias, callbackURL string, maxRetries int, minDelay time.Duration) *TransferTask {
	return &TransferTask{
		Repeater:        task.NewRepeater(maxRetries, minDelay),
		walletID:        walletID,
		from:            fromWallet,
		to:              toAddress,
		amount:          amount,
		callbackURL:     callbackURL,
		statusOnSuccess: statusOnSuccess,
		statusOnFail:    statusOnFail,
		mu:              new(sync.RWMutex),
	}
}

func (t *TransferTask) sendCallback(status consts.CoreStatusAlias) error {
	// Sending callback
	zap.L().Info("Transfer callback",
		zap.String("desired status", string(t.statusOnSuccess)),
		zap.String("actual status", string(status)),
		zap.String("callback url", t.callbackURL),
		zap.String("from", t.from.AddressHex()),
		zap.String("to", t.to),
		zap.String("amount", t.amount.String()),
	)
	err := sendCallback(t.callbackURL, status)
	if err != nil {
		return ErrCallbackRequestFailed
	}

	return nil
}

func (t *TransferTask) Run() error {
	err := t.Iter()
	if err != nil {
		if errors.Is(err, task.ErrMaxRetriesExceeded) {
			if err2 := t.finalize(t.statusOnFail); err2 != nil {
				return err2
			}
		}
		return err
	}

	err = t.from.Transfer(t.amount, t.to)
	defer t.log(err)
	if err != nil {
		return err
	}

	if err = t.finalize(t.statusOnSuccess); err != nil {
		return err
	}

	return nil
}

func (t *TransferTask) Runnables() []task.Runnable {
	t.mu.RLock()
	defer t.mu.RUnlock()
	return []task.Runnable{t}
}

func (t *TransferTask) finalize(alias consts.CoreStatusAlias) error {
	// Updating status in db.
	err := tmpwalletrepo.GlobalRepository.UpdateStatus(context.Background(), t.walletID, alias)
	if err != nil {
		return err
	}

	// Sending status to invoice service.
	err = t.sendCallback(alias)
	if err != nil {
		return err
	}

	return nil
}

func (t *TransferTask) log(err error) {
	fields := []zap.Field{
		zap.Int64("wallet ID", t.walletID),
		zap.Int("retries", t.Retries()),
		zap.String("expectedAmount", t.amount.String()),
		zap.String("currency", string(t.from.Currency())),
		zap.String("from", t.from.AddressHex()),
		zap.String("to", t.to),
	}
	if err == nil {
		zap.L().Info("Transfer processed", fields...)
	} else {
		fields = append(fields, zap.Error(err))
		zap.L().Error("Transfer error", fields...)
	}
}
