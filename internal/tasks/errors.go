package tasks

import "errors"

var (
	ErrCallbackRequestFailed = errors.New("callback request failed")
	ErrUnpaid                = errors.New("invoice is unpaid")
)
