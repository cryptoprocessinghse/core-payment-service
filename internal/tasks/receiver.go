package tasks

import (
	"context"
	"errors"
	"github.com/shopspring/decimal"
	"go.uber.org/zap"
	"processium/payment/internal/processiumcrypto"
	tmpwalletrepo "processium/payment/internal/tmpwallet/impl"
	"processium/payment/pkg/utils/consts"
	"processium/payment/pkg/watchdog/task"
	"sync"
	"time"
)

type PaymentStatus int

const (
	Unpaid PaymentStatus = iota
	Paid
)

var _ task.Runnable = (*ReceiveTask)(nil)

type ReceiveTask struct {
	task.Repeater
	callbackURL     string
	walletID        int64
	wallet          processiumcrypto.Wallet
	expectedAmount  decimal.Decimal
	balance         decimal.Decimal
	statusOnSuccess consts.CoreStatusAlias
	statusOnFail    consts.CoreStatusAlias

	mu *sync.RWMutex
}

func NewReceiveTask(walletID int64, wallet processiumcrypto.Wallet, amount decimal.Decimal, callbackURL string, statusOnSuccess consts.CoreStatusAlias, statusOnFail consts.CoreStatusAlias, maxRetries int, minDelay time.Duration) *ReceiveTask {
	return &ReceiveTask{
		Repeater:        task.NewRepeater(maxRetries, minDelay),
		callbackURL:     callbackURL,
		wallet:          wallet,
		expectedAmount:  amount,
		balance:         decimal.NewFromInt(0),
		walletID:        walletID,
		statusOnSuccess: statusOnSuccess,
		statusOnFail:    statusOnFail,
		mu:              new(sync.RWMutex),
	}
}

func (t *ReceiveTask) inspect() (PaymentStatus, error) {
	t.mu.Lock()
	defer t.mu.Unlock()

	realAmount, err := t.wallet.GetBalance()
	t.balance = realAmount
	if err != nil {
		return Unpaid, err
	}

	if realAmount.LessThan(t.expectedAmount) {
		return Unpaid, nil
	}

	return Paid, nil
}

func (t *ReceiveTask) sendCallback(status consts.CoreStatusAlias) error {
	// Sending callback
	zap.L().Info("Receive callback",
		zap.String("desired status", string(t.statusOnSuccess)),
		zap.String("actual status", string(status)),
		zap.String("callback url", t.callbackURL),
		zap.String("receiver", t.wallet.AddressHex()),
		zap.String("expected amount", t.expectedAmount.String()),
		zap.String("balance", t.balance.String()),
	)
	err := sendCallback(t.callbackURL, status)
	if err != nil {
		return ErrCallbackRequestFailed
	}

	return nil
}

func (t *ReceiveTask) Run() error {
	err := t.Iter()
	if err != nil {
		if errors.Is(err, task.ErrMaxRetriesExceeded) {
			if err2 := t.finalize(t.statusOnFail); err2 != nil {
				return err2
			}
		}
		return err
	}

	paymentStatus, err := t.inspect()
	defer t.log(err)
	if err != nil {
		return err
	}

	if paymentStatus != Paid {
		return ErrUnpaid
	}

	if err = t.finalize(t.statusOnSuccess); err != nil {
		return err
	}

	return nil
}

func (t *ReceiveTask) Runnables() []task.Runnable {
	t.mu.RLock()
	defer t.mu.RUnlock()
	return []task.Runnable{t}
}

func (t *ReceiveTask) finalize(alias consts.CoreStatusAlias) error {
	// Updating status in db.
	err := tmpwalletrepo.GlobalRepository.UpdateStatus(context.Background(), t.walletID, alias)
	if err != nil {
		return err
	}

	// Sending status to invoice service.
	err = t.sendCallback(alias)
	if err != nil {
		return err
	}

	return nil
}

func (t *ReceiveTask) log(err error) {
	fields := []zap.Field{
		zap.Int64("wallet ID", t.walletID),
		zap.Int("retries", t.Retries()),
		zap.Int("max retries", t.MaxRetries()),
		zap.String("expectedAmount", t.expectedAmount.String()),
		zap.String("currency", string(t.wallet.Currency())),
		zap.String("address", t.wallet.AddressHex()),
		zap.String("balance", t.balance.String()),
	}
	if err == nil {
		zap.L().Info("Receive processed", fields...)
	} else {
		fields = append(fields, zap.Error(err))
		zap.L().Error("Receive error", fields...)
	}
}
