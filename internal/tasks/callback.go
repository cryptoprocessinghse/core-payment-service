package tasks

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/url"
	"processium/payment/internal/config"
	"processium/payment/pkg/utils/consts"
	"time"
)

const (
	callbackMethod    = "POST"
	contentTypeHeader = "Content-Type"
	contentTypeValue  = "application/json"
	expectedStatus    = http.StatusOK
)

type callbackBody struct {
	Status consts.CoreStatusAlias `json:"status"`
}

func sendCallback(callbackUrl string, status consts.CoreStatusAlias) error {
	cfg := config.C()

	u, _ := url.JoinPath(cfg.ClientsConfig.Invoice.BaseURL, callbackUrl)
	body, _ := json.Marshal(map[string]any{"status": status})

	for {
		resp, err := http.Post(u, "application/json", bytes.NewBuffer(body))
		if err == nil && resp.StatusCode/100 == 2 {
			break
		}
		// TODO: timer
		time.Sleep(time.Second)
	}

	return nil
}
