package config

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/spf13/viper"
	"processium/payment/pkg/utils/consts"
	"sync"
)

var (
	config Config
	err    error
	once   sync.Once
)

func C() Config {
	return config
}

type Config struct {
	ServerConfig      ServerConfig       `mapstructure:"server_config" validate:"required"`
	PostgresConfig    PostgresConfig     `mapstructure:"postgres_config" validate:"required"`
	MainWalletConfigs []MainWalletConfig `mapstructure:"main_wallet_config" validate:"required"`
	ClientsConfig     ClientsConfig      `mapstructure:"clients_config"`
}

type ServerConfig struct {
	AppVersion string `mapstructure:"app_version"`
	Host       string `mapstructure:"host" validate:"required"`
	Port       int    `mapstructure:"port" validate:"required"`
}

type ClientsConfig struct {
	Invoice struct {
		BaseURL string `mapstructure:"base_url"`
	} `mapstructure:"invoice"`
}

type PostgresConfig struct {
	Host        string `mapstructure:"host" validate:"required"`
	Port        int    `mapstructure:"port" validate:"required"`
	User        string `mapstructure:"user" validate:"required"`
	DbName      string `mapstructure:"db_name" validate:"required"`
	PasswordEnv string `mapstructure:"password_env"`
	Password    string `mapstructure:"password" validate:"required"`
	SSLMode     string `mapstructure:"ssl_mode" validate:"required"`
	CAPath      string `mapstructure:"ca_path" validate:"required"`
}

type MainWalletConfig struct {
	Address    string               `mapstructure:"address" validate:"required"`
	PrivateKey string               `mapstructure:"private_key" validate:"required"`
	Currency   consts.CurrencyAlias `mapstructure:"currency" validate:"required"`
}

func ParseConfig(viperInstance *viper.Viper) (Config, error) {
	once.Do(func() {
		err = viperInstance.Unmarshal(&config)

		if config.PostgresConfig.PasswordEnv != "" {
			config.PostgresConfig.Password = viperInstance.GetString(config.PostgresConfig.PasswordEnv)
		}

		err = validator.New().Struct(config)
	})

	return config, err
}

func (pgCfg *PostgresConfig) ToConnectionString() string {
	return fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=%s target_session_attrs=read-write",
		pgCfg.Host,
		pgCfg.Port,
		pgCfg.User,
		pgCfg.Password,
		pgCfg.DbName,
		pgCfg.SSLMode,
	)
}
