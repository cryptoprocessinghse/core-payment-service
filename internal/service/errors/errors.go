package errors

import (
	"errors"
	"fmt"
	"processium/payment/pkg/utils/consts"
)

var (
	ErrPipelineCreationFailed                   = errors.New("pipeline creation failed")
	ErrTemporaryWalletDoesNotExist              = errors.New("temp wallet does not exist")
	ErrTemporaryWalletCreation                  = errors.New("temp wallet creation error")
	ErrTooSmallAmount                           = fmt.Errorf("too small amount. Minimal is %s", consts.MinExpectedAmount.String())
	ErrTooSmallTotalCommission                  = fmt.Errorf("too small total commission. Minimal is %s", consts.MinTotalCommission.String())
	ErrTotalCommissionIsBiggerOrEqualThanAmount = errors.New("total commission is bigger than amount")
	ErrBigNetCom                                = errors.New("netcom is bigger than total commission")
)
