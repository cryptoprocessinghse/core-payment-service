package service

import (
	"context"
	"github.com/shopspring/decimal"
	"processium/payment/internal/processiumcrypto"
	"processium/payment/pkg/utils/consts"
)

func (ps *PaymentService) GetNetCom3(ctx context.Context, currency consts.CurrencyAlias) (decimal.Decimal, error) {
	w, err := processiumcrypto.GenerateWallet(currency)
	if err != nil {
		return decimal.Decimal{}, err
	}

	return w.NetComN(3)
}
