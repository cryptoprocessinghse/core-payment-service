package service

import (
	"context"
	"errors"
	"github.com/guregu/null"
	"github.com/shopspring/decimal"
	"processium/payment/internal/models"
	"processium/payment/internal/processiumcrypto"
	"processium/payment/internal/requests"
	serviceerrors "processium/payment/internal/service/errors"
	"processium/payment/internal/service/pipeline"
	"processium/payment/pkg/utils/consts"
	"processium/payment/pkg/watchdog"
)

func (ps *PaymentService) CreateTx(ctx context.Context, req *requests.CreateTx) (*models.TemporaryWalletInfo, error) {
	if err := ps.validateTx(ctx, req); err != nil {
		return nil, err
	}

	wallet, err := processiumcrypto.GenerateWallet(req.Currency)
	if err != nil {
		return nil, errors.Join(err, serviceerrors.ErrTemporaryWalletCreation)
	}

	tmpWallet := &models.TemporaryWallet{
		Address:         null.StringFrom(wallet.AddressHex()),
		PrivateKey:      null.StringFrom(wallet.PrivateKey()),
		Currency:        req.Currency,
		Status:          consts.Awaiting,
		ExpectedAmount:  decimal.NewNullDecimal(req.Amount),
		CallbackURL:     null.StringFrom(req.CallbackURL),
		TotalCommission: decimal.NewNullDecimal(req.TotalCommission),
		MerchantAddress: null.StringFrom(req.To),
	}
	err = ps.postgresStorage.CreateTx(ctx, tmpWallet)
	if err != nil {
		return nil, errors.Join(err, serviceerrors.ErrTemporaryWalletCreation)
	}

	pipe, err := pipeline.BuildPipeline(tmpWallet)
	if err != nil {
		return nil, errors.Join(err, serviceerrors.ErrPipelineCreationFailed)
	}

	watchdog.W().PushBack(pipe)

	return &models.TemporaryWalletInfo{
		ID:      tmpWallet.ID,
		Address: tmpWallet.Address,
		Status:  tmpWallet.Status,
	}, nil
}

func (ps *PaymentService) validateTx(ctx context.Context, req *requests.CreateTx) error {
	if req.Amount.LessThan(consts.MinExpectedAmount) {
		return serviceerrors.ErrTooSmallAmount
	}

	if req.TotalCommission.LessThan(consts.MinTotalCommission) {
		return serviceerrors.ErrTooSmallTotalCommission
	}

	if req.TotalCommission.GreaterThanOrEqual(req.Amount) {
		return serviceerrors.ErrTotalCommissionIsBiggerOrEqualThanAmount
	}

	wallet, err := processiumcrypto.GenerateWallet(req.Currency)
	if err != nil {
		return errors.Join(err, serviceerrors.ErrTemporaryWalletCreation)
	}

	netcom3, err := wallet.NetComN(3)
	if err != nil {
		return errors.Join(err, serviceerrors.ErrTemporaryWalletCreation)
	}

	if req.TotalCommission.LessThan(netcom3) {
		return serviceerrors.ErrBigNetCom
	}

	return nil
}
