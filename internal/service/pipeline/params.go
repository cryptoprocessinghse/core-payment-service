package pipeline

import (
	"processium/payment/internal/models"
	"processium/payment/internal/processiumcrypto"
)

type BuildParams struct {
	TmpWalletModel *models.TemporaryWallet

	MainWallet     processiumcrypto.Wallet
	TmpWallet      processiumcrypto.Wallet
	MerchantWallet processiumcrypto.Wallet
}
