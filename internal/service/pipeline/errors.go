package pipeline

import "errors"

var (
	ErrNoPipelineForStatus = errors.New("no pipeline for status")
)
