package pipeline

import (
	"processium/payment/pkg/utils/consts"
	"processium/payment/pkg/watchdog/task"
)

type Builder func(params *BuildParams) (*task.Pipeline, error)

func Unimplemented() Builder {
	return func(params *BuildParams) (*task.Pipeline, error) {
		return nil, ErrNoPipelineForStatus
	}
}

var (
	mapping = map[consts.CoreStatusAlias]Builder{
		consts.Awaiting:  buildPipelineFromAwaiting,
		consts.Held:      buildPipelineFromHeld,
		consts.Sending:   buildPipelineFromSending,
		consts.Succeed:   buildPipelineFromSucceed,
		consts.Clear:     Unimplemented(),
		consts.Cancelled: Unimplemented(),
		consts.Failed:    Unimplemented(),
	}
)

func getBuilder(status consts.CoreStatusAlias) Builder {
	if b, ok := mapping[status]; !ok {
		return Unimplemented()
	} else {
		return b
	}
}
