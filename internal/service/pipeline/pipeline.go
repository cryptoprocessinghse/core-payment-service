package pipeline

import (
	"errors"
	"processium/payment/internal/models"
	"processium/payment/internal/processiumcrypto"
	serviceerrors "processium/payment/internal/service/errors"
	"processium/payment/internal/tasks"
	"processium/payment/pkg/utils/consts"
	"processium/payment/pkg/watchdog/task"
)

func BuildPipeline(tmpWalletModel *models.TemporaryWallet) (*task.Pipeline, error) {
	tmpWallet, err := processiumcrypto.GenerateWallet(
		tmpWalletModel.Currency,
		tmpWalletModel.Address.ValueOrZero(),
		tmpWalletModel.PrivateKey.ValueOrZero(),
	)
	if err != nil {
		return nil, errors.Join(err, serviceerrors.ErrTemporaryWalletCreation)
	}

	mainWallet, err := processiumcrypto.MainWallet(tmpWalletModel.Currency)
	if err != nil {
		return nil, errors.Join(err, serviceerrors.ErrTemporaryWalletCreation)
	}

	merchWallet, err := processiumcrypto.GenerateWallet(tmpWalletModel.Currency, tmpWalletModel.MerchantAddress.ValueOrZero(), consts.EmptyPrivateKey)
	if err != nil {
		return nil, errors.Join(err, serviceerrors.ErrTemporaryWalletCreation)
	}

	params := BuildParams{
		MainWallet:     mainWallet,
		TmpWalletModel: tmpWalletModel,
		TmpWallet:      tmpWallet,
		MerchantWallet: merchWallet,
	}

	return getBuilder(tmpWalletModel.Status)(&params)
}

func buildPipelineFromAwaiting(params *BuildParams) (*task.Pipeline, error) {
	pipeline := task.NewPipeline()

	rRunnable := tasks.NewReceiveTask(
		params.TmpWalletModel.ID.Int64,
		params.TmpWallet,
		params.TmpWalletModel.ExpectedAmount.Decimal,
		params.TmpWalletModel.CallbackURL.ValueOrZero(),
		consts.Held,
		consts.Failed,
		consts.DefaultMaxRetries,
		consts.DefaultDelay,
	)
	pipeline.Extend(rRunnable)

	heldPipe, err := buildPipelineFromHeld(params)
	if err != nil {
		return nil, err
	}
	pipeline.Extend(heldPipe)

	return pipeline, nil
}

func buildPipelineFromHeld(params *BuildParams) (*task.Pipeline, error) {
	pipeline := task.NewPipeline()

	amount, err := params.MainWallet.NetComN(2)
	if err != nil {
		return nil, err
	}

	tRunnable := tasks.NewTransferTask(
		params.TmpWalletModel.ID.Int64,
		params.MainWallet,
		params.TmpWalletModel.Address.ValueOrZero(),
		amount,
		consts.Sending,
		consts.Failed,
		params.TmpWalletModel.CallbackURL.ValueOrZero(),
		0,
		consts.MinDelay,
	)
	pipeline.Extend(tRunnable)

	rRunnable := tasks.NewReceiveTask(
		params.TmpWalletModel.ID.Int64,
		params.TmpWallet,
		amount.Add(params.TmpWalletModel.ExpectedAmount.Decimal),
		params.TmpWalletModel.CallbackURL.ValueOrZero(),
		consts.Sending,
		consts.Failed,
		0,
		consts.MinDelay,
	)
	pipeline.Extend(rRunnable)

	sendingPipe, err := buildPipelineFromSending(params)
	if err != nil {
		return nil, err
	}
	pipeline.Extend(sendingPipe)

	return pipeline, nil
}

func buildPipelineFromSending(params *BuildParams) (*task.Pipeline, error) {
	pipeline := task.NewPipeline()

	tRunnable := tasks.NewTransferTask(
		params.TmpWalletModel.ID.Int64,
		params.TmpWallet,
		params.TmpWalletModel.MerchantAddress.ValueOrZero(),
		params.TmpWalletModel.ExpectedAmount.Decimal.Sub(params.TmpWalletModel.TotalCommission.Decimal),
		consts.Succeed,
		consts.Failed,
		params.TmpWalletModel.CallbackURL.ValueOrZero(),
		0,
		consts.MinDelay,
	)
	pipeline.Extend(tRunnable)

	succeedPipe, err := buildPipelineFromSucceed(params)
	if err != nil {
		return nil, err
	}
	pipeline.Extend(succeedPipe)

	return pipeline, nil
}

func buildPipelineFromSucceed(params *BuildParams) (*task.Pipeline, error) {
	pipeline := task.NewPipeline()

	tRunnable := tasks.NewTransferTask(
		params.TmpWalletModel.ID.Int64,
		params.TmpWallet,
		params.MainWallet.AddressHex(),
		params.TmpWalletModel.TotalCommission.Decimal,
		consts.Clear,
		consts.Succeed,
		params.TmpWalletModel.CallbackURL.ValueOrZero(),
		0,
		consts.MinDelay,
	)
	pipeline.Extend(tRunnable)

	return pipeline, nil
}
