package service

import (
	"context"
	"errors"
	"processium/payment/internal/models"
	"processium/payment/internal/processiumcrypto"
	serviceerrors "processium/payment/internal/service/errors"
)

func (ps *PaymentService) GetTx(ctx context.Context, walletID int) (*models.TemporaryWalletInfo, error) {
	// Getting wallet
	tmpWallet, err := ps.postgresStorage.GetTx(ctx, walletID)
	if err != nil {
		return nil, errors.Join(serviceerrors.ErrTemporaryWalletDoesNotExist, err)
	}

	// Counting balance.
	wallet, err := processiumcrypto.GenerateWallet(tmpWallet.Currency, tmpWallet.Address.ValueOrZero(), "")
	if err != nil {
		return nil, err
	}

	amount, err := wallet.GetBalance()
	if err != nil {
		return nil, err
	}

	return &models.TemporaryWalletInfo{
		ID:      tmpWallet.ID,
		Address: tmpWallet.Address,
		Status:  tmpWallet.Status,
		Balance: models.Balance{
			Currency: tmpWallet.Currency,
			Amount:   amount,
		},
	}, nil
}
