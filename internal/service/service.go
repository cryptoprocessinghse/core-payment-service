package service

import (
	"context"
	"go.uber.org/zap"
	"processium/payment/internal/service/pipeline"
	"processium/payment/internal/storage/postgres/impl"
	tmpwalletrepo "processium/payment/internal/tmpwallet/impl"
	"processium/payment/pkg/utils/consts"
	"processium/payment/pkg/utils/errors"
	"processium/payment/pkg/watchdog"
)

type PaymentService struct {
	postgresStorage *impl.PGStorage
}

func NewService(postgresStorage *impl.PGStorage) *PaymentService {
	return &PaymentService{
		postgresStorage: postgresStorage,
	}
}

func (ps *PaymentService) Init() error {
	tWallets, err := tmpwalletrepo.GlobalRepository.SelectByStatusList(context.TODO(), consts.StatusesToBeCollected())
	if err != nil {
		return err
	}

	errCnt := 0
	for _, w := range tWallets {
		p, err := pipeline.BuildPipeline(w)
		if err != nil && !errors.AnyOf(err, pipeline.ErrNoPipelineForStatus) {
			errCnt += 1
			zap.L().Error("Error while collecting temporary wallet!", zap.Error(err), zap.Int64("ID", w.ID.Int64))
			continue
		}

		watchdog.W().PushBack(p)
	}
	zap.L().Info("Temporary wallets collected!", zap.Int("collected", len(tWallets)), zap.Int("errors", errCnt))
	return nil
}
