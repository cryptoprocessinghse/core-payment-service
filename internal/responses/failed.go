package responses

type FailedResponse struct {
	Message string `json:"message"`
}
