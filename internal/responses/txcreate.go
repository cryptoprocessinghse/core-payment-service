package responses

type CreateTxSucceed struct {
	ID      int64  `json:"id"`
	Address string `json:"address"`
}
