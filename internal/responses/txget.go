package responses

import (
	"processium/payment/internal/models"
)

type GetTxSucceed struct {
	WalletInfo models.TemporaryWalletInfo `json:"walletInfo"`
}
