package errors

import "errors"

var (
	ErrNotFound = errors.New("currency not found")
	ErrDbError  = errors.New("db error")
)
