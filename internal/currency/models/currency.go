package models

import (
	"github.com/guregu/null"
	"github.com/shopspring/decimal"
)

type Currency struct {
	Name           null.String         `db:"name" ,json:"name"`
	BaseCommission decimal.NullDecimal `db:"base_commission" ,json:"base_commission"`
	Decimals       null.Int            `db:"decimals" ,json:"decimals"`
	IsToken        null.Bool           `db:"is_token" ,json:"is_token"`
	NetworkID      null.Int            `db:"network_id" ,json:"network_id"`
}
