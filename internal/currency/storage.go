package currency

import (
	"context"
	"processium/payment/internal/currency/models"
	"processium/payment/pkg/utils/consts"
)

type Storage interface {
	read
}

type read interface {
	GetCurrencyByName(context.Context, consts.CurrencyAlias) (*models.Currency, error)
}
