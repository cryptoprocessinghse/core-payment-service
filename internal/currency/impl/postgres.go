package impl

import (
	"context"
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	"processium/payment/internal/currency"
	currencyerrors "processium/payment/internal/currency/errors"
	"processium/payment/internal/currency/models"
	"processium/payment/internal/currency/queries"
	"processium/payment/pkg/utils/consts"
	"sync"
)

var (
	GlobalRepository     *PgRepositoryImpl
	validCurrenciesCache sync.Map
	creationMutex        = &sync.Mutex{}
)

func NewRepository(db *sqlx.DB) *PgRepositoryImpl {
	return &PgRepositoryImpl{DB: db}
}

func InitGlobalRepository(db *sqlx.DB) *PgRepositoryImpl {
	creationMutex.Lock()
	defer creationMutex.Unlock()

	if GlobalRepository == nil {
		GlobalRepository = NewRepository(db)
	}

	return GlobalRepository
}

type PgRepositoryImpl struct {
	DB *sqlx.DB
}

func (r *PgRepositoryImpl) GetCurrencyByName(ctx context.Context, name consts.CurrencyAlias) (*models.Currency, error) {
	if currencyCache, ok := validCurrenciesCache.Load(name); ok {
		return currencyCache.(*models.Currency), nil
	}

	result := new(models.Currency)
	err := r.DB.GetContext(ctx, result, queries.QueryGetCurrencyByID, name)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, currencyerrors.ErrNotFound
		}
		return nil, errors.Join(currencyerrors.ErrDbError, err)
	}

	validCurrenciesCache.Store(name, result)
	return result, nil
}

var _ currency.Storage = (*PgRepositoryImpl)(nil)
