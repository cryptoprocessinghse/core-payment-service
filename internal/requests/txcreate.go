package requests

import (
	"github.com/shopspring/decimal"
	"processium/payment/pkg/utils/consts"
)

type CreateTx struct {
	Currency        consts.CurrencyAlias `json:"currency" validate:"required"`
	Amount          decimal.Decimal      `json:"amount" validate:"required"`
	TotalCommission decimal.Decimal      `json:"totalCommission" validate:"required"`
	CallbackURL     string               `json:"callbackURL" validate:"required"`
	To              string               `json:"to" validate:"required"`
}
