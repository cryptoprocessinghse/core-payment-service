package processiumcrypto

import (
	"github.com/shopspring/decimal"
	"processium/payment/pkg/utils/calculations"
	"processium/payment/pkg/utils/consts"
)

type Wallet interface {
	GetBalance() (decimal.Decimal, error)
	Transfer(decimal.Decimal, string) error

	// NetComN n * GasPrice * GasLimit2
	// GasLimit2 = GasLimit +  GasLimit*min((|VolatileRate| + buffer)/2, buffer))
	NetComN(n float64) (decimal.Decimal, error)

	AddressHex() string
	PrivateKey() string
	SetAddressHex(string)
	SetPrivateKey(string)
	Currency() consts.CurrencyAlias
}

// walletImpl embed this struct to nest PrivateKey, PublicKey and Humanize realisation.
type walletImpl struct {
	currency   consts.CurrencyAlias
	privateKey string
	addressHex string
}

func (wi *walletImpl) AddressHex() string {
	return wi.addressHex
}

func (wi *walletImpl) PrivateKey() string {
	return wi.privateKey
}

func (wi *walletImpl) SetAddressHex(addressHex string) {
	wi.addressHex = addressHex
}

func (wi *walletImpl) SetPrivateKey(privateKey string) {
	wi.privateKey = privateKey
}

func (wi *walletImpl) Currency() consts.CurrencyAlias {
	return wi.currency
}

func (wi *walletImpl) GasLimitAmortized(volatileRate decimal.Decimal) decimal.Decimal {
	var (
		gasLimit = decimal.NewFromInt(21000)
		buffer   = decimal.NewFromFloat(0.1)
		two      = decimal.NewFromInt(2)
		one      = decimal.NewFromInt(1)
	)

	abs := calculations.DecimalAbs(volatileRate)
	buff := decimal.Min(abs.Add(buffer).Div(two), buffer)
	return gasLimit.Mul(one.Add(buff))
}
