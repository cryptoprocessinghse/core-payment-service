package processiumcrypto

import "errors"

var (
	ErrUnsupportedCurrency      = errors.New("unsupported currency")
	ErrNotAddress               = errors.New("not address")
	ErrKeysCountMismatch        = errors.New("wallet keys should be a pair")
	ErrWalletGeneration         = errors.New("wallet generation error")
	ErrBlockchainNodeConnection = errors.New("blockchain connection error")
	ErrCurrencyMismatch         = errors.New("currencies mismatch")
	ErrNetworkMismatch          = errors.New("network mismatch")
	ErrPossibleDivisionByZero   = errors.New("possible division by zero")
	ErrUnprocessablePrivateKey  = errors.New("unprocessable private key")
)
