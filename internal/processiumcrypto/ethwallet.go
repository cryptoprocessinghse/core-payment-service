package processiumcrypto

import (
	"context"
	"crypto/ecdsa"
	"errors"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/shopspring/decimal"
	"math/big"
	currencyrepo "processium/payment/internal/currency/impl"
	networkrepo "processium/payment/internal/network/impl"
	"processium/payment/pkg/utils/calculations"
	"processium/payment/pkg/utils/consts"
)

type ETHWallet struct {
	walletImpl
}

func generateETHWallet(keys ...string) (Wallet, error) {
	if len(keys) == 2 {
		if !common.IsHexAddress(keys[0]) {
			return nil, ErrNotAddress
		}

		return &ETHWallet{
			walletImpl{
				addressHex: keys[0],
				privateKey: keys[1],
				currency:   consts.ETH,
			},
		}, nil
	} else if len(keys) == 0 {
		privateKeyECSDA, err := crypto.GenerateKey()
		if err != nil {
			return nil, errors.Join(ErrWalletGeneration, err)
		}
		publicKey := privateKeyECSDA.Public()
		publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)

		if !ok {
			return nil, ErrWalletGeneration
		}

		privateKeyHumanized := hexutil.Encode(crypto.FromECDSA(privateKeyECSDA))[2:]
		addressHex := crypto.PubkeyToAddress(*publicKeyECDSA).Hex()

		return &ETHWallet{
			walletImpl{
				addressHex: addressHex,
				privateKey: privateKeyHumanized,
				currency:   consts.ETH,
			},
		}, nil
	} else {
		return nil, ErrKeysCountMismatch
	}
}

func (w ETHWallet) GetBalance() (decimal.Decimal, error) {
	cur, err := currencyrepo.GlobalRepository.GetCurrencyByName(context.Background(), w.currency)
	if err != nil {
		return decimal.Decimal{}, errors.Join(err, ErrCurrencyMismatch)
	}

	network, err := networkrepo.GlobalRepository.GetNetworkByID(context.Background(), int(cur.NetworkID.ValueOrZero()))
	if err != nil {
		return decimal.Decimal{}, errors.Join(err, ErrNetworkMismatch)
	}

	client, err := ethclient.Dial(network.Address.ValueOrZero())
	if err != nil {
		return decimal.Decimal{}, errors.Join(err, ErrBlockchainNodeConnection)
	}

	balance, err := client.BalanceAt(context.Background(), common.HexToAddress(w.AddressHex()), nil)
	if err != nil {
		return decimal.Decimal{}, errors.Join(err, ErrBlockchainNodeConnection)
	}

	divisor := int32(cur.Decimals.ValueOrZero())
	if divisor == 0 {
		return decimal.Decimal{}, ErrPossibleDivisionByZero
	}

	ethBalance := decimal.NewFromBigInt(balance, 0).
		Div(decimal.NewFromBigInt(big.NewInt(1), divisor))

	return ethBalance, nil
}

func (w ETHWallet) Transfer(amount decimal.Decimal, to string) error {
	cur, err := currencyrepo.GlobalRepository.GetCurrencyByName(context.Background(), w.currency)
	if err != nil {
		return errors.Join(err, ErrCurrencyMismatch)
	}

	network, err := networkrepo.GlobalRepository.GetNetworkByID(context.Background(), int(cur.NetworkID.ValueOrZero()))
	if err != nil {
		return errors.Join(err, ErrNetworkMismatch)
	}

	client, err := ethclient.Dial(network.Address.ValueOrZero())
	if err != nil {
		return errors.Join(err, ErrBlockchainNodeConnection)
	}

	privateKey, err := crypto.HexToECDSA(w.PrivateKey())
	if err != nil {
		return ErrUnprocessablePrivateKey
	}

	toAddress := common.HexToAddress(to)

	// Create a new transaction
	nonce, err := client.PendingNonceAt(context.Background(), common.HexToAddress(w.AddressHex()))
	if err != nil {
		return errors.Join(err, ErrBlockchainNodeConnection)
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		return errors.Join(err, ErrBlockchainNodeConnection)
	}

	// TODO: Амортизация
	gasLimit := w.GasLimitAmortized(decimal.NewFromInt(0)).BigInt().Uint64()

	// Create a new transaction object
	tx := types.NewTx(&types.LegacyTx{
		Nonce:    nonce,
		GasPrice: gasPrice,
		Gas:      gasLimit,
		To:       &toAddress,
		Value:    amount.Mul(calculations.DecimalDelimeterByDigits(int32(cur.Decimals.Int64))).BigInt(),
		Data:     nil,
		V:        nil,
		R:        nil,
		S:        nil,
	})

	// Sign the transaction
	signedTx, err := types.SignTx(tx, types.HomesteadSigner{}, privateKey)
	if err != nil {
		return errors.Join(err, ErrBlockchainNodeConnection)
	}

	// Send the transaction
	err = client.SendTransaction(context.Background(), signedTx)
	if err != nil {
		return errors.Join(err, ErrBlockchainNodeConnection)
	}
	return nil
}

func (w ETHWallet) NetComN(n float64) (decimal.Decimal, error) {
	if currencyrepo.GlobalRepository == nil {
		return decimal.Decimal{}, ErrCurrencyMismatch
	}

	cur, err := currencyrepo.GlobalRepository.GetCurrencyByName(context.Background(), w.currency)
	if err != nil {
		return decimal.Decimal{}, errors.Join(ErrCurrencyMismatch, err)
	}

	network, err := networkrepo.GlobalRepository.GetNetworkByID(context.Background(), int(cur.NetworkID.ValueOrZero()))
	if err != nil {
		return decimal.Decimal{}, errors.Join(ErrNetworkMismatch, err)
	}

	client, err := ethclient.Dial(network.Address.ValueOrZero())
	if err != nil {
		return decimal.Decimal{}, errors.Join(ErrBlockchainNodeConnection, err)
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		return decimal.Decimal{}, errors.Join(ErrBlockchainNodeConnection, err)
	}

	divisor := int32(cur.Decimals.ValueOrZero())
	if divisor == 0 {
		return decimal.Decimal{}, ErrPossibleDivisionByZero
	}
	gasPriceInNative := decimal.NewFromBigInt(gasPrice, 0).Div(decimal.NewFromBigInt(big.NewInt(1), divisor))

	// TODO: Volatile rate per last minute
	return w._netcomn(gasPriceInNative, decimal.NewFromFloat(0), n), nil
}

func (w ETHWallet) _netcomn(gasPrice, volatileRate decimal.Decimal, n float64) decimal.Decimal {
	var nn = decimal.NewFromFloat(n)
	return gasPrice.Mul(w.GasLimitAmortized(volatileRate)).Mul(nn)
}

var _ Wallet = (*ETHWallet)(nil)
