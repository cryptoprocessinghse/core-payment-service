package processiumcrypto

import (
	"processium/payment/internal/config"
	"processium/payment/pkg/utils/consts"
)

func MainWallet(currency consts.CurrencyAlias) (Wallet, error) {
	// Find needed wallet
	for _, v := range config.C().MainWalletConfigs {
		if v.Currency == currency {
			return GenerateWallet(currency, v.Address, v.PrivateKey)
		}
	}
	return nil, ErrWalletGeneration
}
