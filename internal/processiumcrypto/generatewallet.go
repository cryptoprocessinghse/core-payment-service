package processiumcrypto

import "processium/payment/pkg/utils/consts"

type generator func(keys ...string) (Wallet, error)

var (
	mapper = map[consts.CurrencyAlias]generator{
		consts.BNB:       generateBNBWallet,
		consts.ETHGoerli: generateETHGWallet,
	}
)

func GenerateWallet(alias consts.CurrencyAlias, keys ...string) (Wallet, error) {
	gen, ok := mapper[alias]
	if !ok {
		return nil, ErrUnsupportedCurrency
	}

	return gen(keys...)
}
