package queries

const CreateTx = `
INSERT INTO wallets.temporary_wallet (wallet_address, private_key, currency, status, expected_amount, callback_url, total_commission, merchant_address)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id`
