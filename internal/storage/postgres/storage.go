package postgres

import "processium/payment/internal/models"

type Storage interface {
	CreateTx(tmpWallet *models.TemporaryWallet) error
	GetTx(walletID int) (*models.TemporaryWallet, error)
}
