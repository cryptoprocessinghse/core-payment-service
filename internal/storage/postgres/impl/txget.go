package impl

import (
	"context"
	"errors"
	"processium/payment/internal/models"
	postgreserrors "processium/payment/internal/storage/postgres/errors"
	"processium/payment/internal/storage/postgres/queries"
)

func (pgStorage *PGStorage) GetTx(ctx context.Context, walletID int) (*models.TemporaryWallet, error) {
	result := new(models.TemporaryWallet)
	err := pgStorage.db.GetContext(
		ctx,
		result,
		queries.GetTx,
		walletID,
	)

	if err != nil {
		return nil, errors.Join(postgreserrors.DatabaseError, err)
	}

	return result, nil
}
