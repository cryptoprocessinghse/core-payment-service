package impl

import (
	"github.com/jmoiron/sqlx"
)

type PGStorage struct {
	db *sqlx.DB
}

func NewPGStorage(db *sqlx.DB) (*PGStorage, error) {
	return &PGStorage{db: db}, nil
}

func (pgStorage *PGStorage) Close() error {
	return pgStorage.db.Close()
}
