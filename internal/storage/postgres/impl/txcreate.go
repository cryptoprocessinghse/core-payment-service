package impl

import (
	"context"
	"errors"
	"processium/payment/internal/models"
	postgreserrors "processium/payment/internal/storage/postgres/errors"
	"processium/payment/internal/storage/postgres/queries"
)

func (pgStorage *PGStorage) CreateTx(ctx context.Context, tmpWallet *models.TemporaryWallet) error {
	err := pgStorage.db.QueryRowxContext(
		ctx,
		queries.CreateTx,
		tmpWallet.Address,
		tmpWallet.PrivateKey,
		tmpWallet.Currency,
		tmpWallet.Status,
		tmpWallet.ExpectedAmount,
		tmpWallet.CallbackURL,
		tmpWallet.TotalCommission,
		tmpWallet.MerchantAddress,
	).Scan(&tmpWallet.ID)

	if err != nil {
		return errors.Join(postgreserrors.DatabaseError, err)
	}

	return nil
}
