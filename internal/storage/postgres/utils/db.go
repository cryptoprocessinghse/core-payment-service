package utils

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"os"
	"processium/payment/internal/config"
	"time"
)

func NewPGDb(cfg config.PostgresConfig) (*sqlx.DB, error) {
	connectionURL := cfg.ToConnectionString()

	pgxConnConf, err := pgx.ParseDSN(connectionURL)
	if err != nil {
		return nil, err
	}

	if cfg.SSLMode != "disable" {
		rootCertPool := x509.NewCertPool()
		pem, err := os.ReadFile(cfg.CAPath)
		if err != nil {
			return nil, err
		}

		if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
			return nil, errors.New("failed to append PEM")
		}

		pgxConnConf.TLSConfig = &tls.Config{
			RootCAs:            rootCertPool,
			InsecureSkipVerify: true,
		}
	}

	pool, err := pgx.NewConnPool(pgx.ConnPoolConfig{
		ConnConfig:     pgxConnConf,
		AfterConnect:   nil,
		MaxConnections: 10,
		AcquireTimeout: 30 * time.Second,
	})
	if err != nil {
		return nil, err
	}

	native := stdlib.OpenDBFromPool(pool)
	database := sqlx.NewDb(native, "pgx")

	if err = database.Ping(); err != nil {
		return nil, err
	}
	return database, nil
}
