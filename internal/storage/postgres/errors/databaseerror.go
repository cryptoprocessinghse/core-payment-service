package errors

import (
	errs "errors"
)

var DatabaseError = errs.New("database error")
