package server

import (
	"context"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"processium/payment/internal/config"
	paymenthandlers "processium/payment/internal/server/handlers"
	"processium/payment/internal/service"
	"syscall"
)

type PaymentServer struct {
	fiber   *fiber.App
	config  config.ServerConfig
	service *service.PaymentService
}

func (s *PaymentServer) Run(_ context.Context) error {
	s.fiber.Use(recover.New())

	handlers := paymenthandlers.NewPaymentServerHandlers(s.service)
	if err := s.mapRoutes(s.fiber, handlers); err != nil {
		zap.L().Fatal("Cannot map handlers", zap.Error(err))
	}

	go func() {
		address := fmt.Sprintf("%s:%d", s.config.Host, s.config.Port)

		zap.L().Info("Starting main server!", zap.String("address", address))
		if err := s.fiber.Listen(address); err != nil {
			zap.L().Fatal("Fiber returned an error while starting attempt!", zap.Error(err))
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit

	if err := s.fiber.Shutdown(); err != nil {
		zap.L().Error("Fiber shutdown error!", zap.Error(err))
	} else {
		zap.L().Info("Fiber exited successfully!")
	}

	return nil
}

func NewServer(config config.ServerConfig, service *service.PaymentService) *PaymentServer {
	return &PaymentServer{
		config:  config,
		fiber:   fiber.New(fiber.Config{ErrorHandler: paymenthandlers.ErrorHandler()}),
		service: service,
	}
}
