package handlers

import (
	"errors"
	"github.com/gofiber/fiber/v2"
	"processium/payment/internal/processiumcrypto"
	"processium/payment/internal/responses"
	serviceerrors "processium/payment/internal/service/errors"
)

func ErrorHandler() fiber.ErrorHandler {
	return func(ctx *fiber.Ctx, err error) error {
		if errors.Is(err, processiumcrypto.ErrUnsupportedCurrency) {
			return ctx.Status(fiber.StatusBadRequest).JSON(responses.FailedResponse{Message: "Unsupported currency!"})
		} else if errors.Is(err, processiumcrypto.ErrNotAddress) {
			return ctx.Status(fiber.StatusBadRequest).JSON(responses.FailedResponse{Message: "Specified address is not hex!"})
		} else if errors.Is(err, serviceerrors.ErrTemporaryWalletDoesNotExist) {
			return ctx.Status(fiber.StatusBadRequest).JSON(responses.FailedResponse{Message: "Temporary wallet does not exist!"})
		} else if errors.Is(err, serviceerrors.ErrBigNetCom) {
			return ctx.Status(fiber.StatusBadRequest).JSON(responses.FailedResponse{Message: "Net commission is bigger than amount!"})
		} else if errors.Is(err, processiumcrypto.ErrWalletGeneration) {
			return ctx.Status(fiber.StatusBadRequest).JSON(responses.FailedResponse{Message: "Error while fetching main wallet!"})
		}

		return ctx.Status(fiber.StatusInternalServerError).JSON(responses.FailedResponse{Message: err.Error()})
	}
}
