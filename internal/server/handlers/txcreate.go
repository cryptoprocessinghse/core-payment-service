package handlers

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
	"processium/payment/internal/requests"
	"processium/payment/internal/responses"
)

func (psh *PaymentServerHandlers) CreateTx() fiber.Handler {
	return func(c *fiber.Ctx) error {
		req := new(requests.CreateTx)
		if err := c.BodyParser(req); err != nil || req == nil {
			zap.L().Error("Request parsing error", zap.Error(err))
			return c.Status(fiber.StatusBadRequest).JSON(responses.FailedResponse{Message: err.Error()})
		}

		err := validator.New().Struct(req)
		if err != nil {
			zap.L().Error("Request validation error", zap.Error(err))
			return c.Status(fiber.StatusBadRequest).JSON(responses.FailedResponse{Message: err.Error()})
		}

		wallet, err := psh.paymentService.CreateTx(c.Context(), req)
		if err != nil {
			zap.L().Error("Caught an error", zap.Error(err))
			return err
		}

		return c.Status(fiber.StatusCreated).JSON(responses.CreateTxSucceed{
			ID:      wallet.ID.ValueOrZero(),
			Address: wallet.Address.ValueOrZero(),
		})
	}
}
