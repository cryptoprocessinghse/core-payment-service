package handlers

import (
	"github.com/gofiber/fiber/v2"
	"processium/payment/internal/responses"
	"strconv"
)

func (psh *PaymentServerHandlers) GetTx() fiber.Handler {
	return func(c *fiber.Ctx) (err error) {
		tempWalletIDValue := c.Params("id", "")
		tempWalletID, err := strconv.Atoi(tempWalletIDValue)
		if err != nil {
			c.Status(fiber.StatusNotFound).JSON(responses.FailedResponse{Message: "Wrong id!"})
		}

		walletInfo, err := psh.paymentService.GetTx(c.Context(), tempWalletID)
		if err != nil {
			return err
		}

		return c.Status(fiber.StatusOK).JSON(responses.GetTxSucceed{
			WalletInfo: *walletInfo,
		})
	}
}
