package handlers

import (
	"processium/payment/internal/service"
)

type PaymentServerHandlers struct {
	paymentService *service.PaymentService
}

func NewPaymentServerHandlers(service *service.PaymentService) *PaymentServerHandlers {
	return &PaymentServerHandlers{paymentService: service}
}
