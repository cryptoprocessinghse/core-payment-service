package server

import (
	"github.com/gofiber/contrib/fiberzap"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"go.uber.org/zap"
	"processium/payment/internal/server/handlers"
	"processium/payment/pkg/utils/server"
)

func (s *PaymentServer) mapRoutes(router fiber.Router, h *handlers.PaymentServerHandlers) error {
	router.Use(fiberzap.New(fiberzap.Config{Logger: zap.L()}))
	router.Use(recover.New())
	router.Get("/ping", server.PingHandler())

	v1Group := router.Group("v1")
	v1Group.Post("/tx", h.CreateTx())
	v1Group.Get("/tx/:id", h.GetTx())
	return nil
}
