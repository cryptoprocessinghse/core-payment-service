package models

import (
	"github.com/guregu/null"
)

type Network struct {
	ID      null.Int    `db:"id" ,json:"id"`
	Name    null.String `db:"name" ,json:"name"`
	Address null.String `db:"node_address" ,json:"address"`
}
