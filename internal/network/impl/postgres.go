package impl

import (
	"context"
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	"processium/payment/internal/network"
	networkerrors "processium/payment/internal/network/errors"
	"processium/payment/internal/network/models"
	"processium/payment/internal/network/queries"
	"sync"
)

var (
	GlobalRepository   *PgRepositoryImpl
	validNetworksCache sync.Map
	creationMutex      = &sync.Mutex{}
)

func NewRepository(db *sqlx.DB) *PgRepositoryImpl {
	return &PgRepositoryImpl{DB: db}
}

func InitGlobalRepository(db *sqlx.DB) *PgRepositoryImpl {
	creationMutex.Lock()
	defer creationMutex.Unlock()

	if GlobalRepository == nil {
		GlobalRepository = NewRepository(db)
	}

	return GlobalRepository
}

type PgRepositoryImpl struct {
	DB *sqlx.DB
}

func (r *PgRepositoryImpl) GetNetworkByID(ctx context.Context, ID int) (*models.Network, error) {
	if currencyCache, ok := validNetworksCache.Load(ID); ok {
		return currencyCache.(*models.Network), nil
	}

	result := new(models.Network)
	err := r.DB.GetContext(ctx, result, queries.GetNetworkByID, ID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, networkerrors.ErrNotFound
		}
		return nil, errors.Join(networkerrors.ErrDbError, err)
	}

	validNetworksCache.Store(ID, result)
	return result, nil
}

var _ network.Storage = (*PgRepositoryImpl)(nil)
