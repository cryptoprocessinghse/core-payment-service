package network

import (
	"context"
	"processium/payment/internal/network/models"
)

type Storage interface {
	read
}

type read interface {
	GetNetworkByID(context.Context, int) (*models.Network, error)
}
